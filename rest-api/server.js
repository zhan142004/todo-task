const express = require("express");
const cors = require("cors");
const todoRoutes = require("./src/todo/routes");

const app = express();
const port = 3000;

app.use(express.json());
app.use(cors());

app.get("/", (req, res) => {
  res.send("Hello world!");
});

app.use("/api/v1/todos", todoRoutes);

app.listen(port, () => console.log(`app listening on port ${port}`));
