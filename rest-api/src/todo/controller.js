const pool = require("../../db");
const queries = require("./queries");

const getTodos = (req, res) => {
  pool.query(queries.getTodos, (error, results) => {
    if (error) throw error;
    if (!results.rows.length) {
      return res.status(404).json({
        status: "not-found",
        message: "there is no todos",
      });
    }
    res.status(200).json(results.rows);
  });
};

const getTodoById = (req, res) => {
  const id = parseInt(req.params.id);
  pool.query(queries.getTodoById, [id], (error, results) => {
    if (error) throw error;
    const noTodoFound = !results.rows.length;
    if (noTodoFound) {
      return res.status(404).json({
        status: "not-found",
        message: "there is no todo with that id in the database",
      });
    }
    res.status(200).json(results.rows);
  });
};

const addTodo = (req, res) => {
  const { title, description, is_completed, in_progress, is_todo } = req.body;

  if (!title || !description) {
    res.status(400).json("title and description required");
  }

  pool.query(
    queries.addTodo,
    [title, description, is_completed, in_progress, is_todo],
    (error, results) => {
      if (error) throw error;
      res.status(201).send("Todo created successfully");
    }
  );
};

const removeTodo = (req, res) => {
  const id = parseInt(req.params.id);

  pool.query(queries.getTodoById, [id], (error, results) => {
    const noTodoFound = !results.rows.length;
    if (noTodoFound) {
      return res.status(404).json({
        status: "not-found",
        message: "there is no todo with that id in the database",
      });
    }

    pool.query(queries.removeTodo, [id], (error, results) => {
      if (error) throw error;
      res.status(200).send("Todo removed successfully");
    });
  });
};

const updateTodo = (req, res) => {
  const id = parseInt(req.params.id);
  const { title, description, is_completed, in_progress, is_todo } = req.body;

  pool.query(queries.getTodoById, [id], (error, results) => {
    const noTodoFound = !results.rows.length;
    if (noTodoFound) {
      return res.status(404).json({
        status: "not-found",
        message: "there is no todo with that id in the database",
      });
    }

    pool.query(
      queries.updateTodo,
      [title, description, is_completed, in_progress, is_todo, id],
      (error, results) => {
        if (error) throw error;
        res.status(200).send("Todo updated successfully");
      }
    );
  });
};

module.exports = {
  getTodos,
  getTodoById,
  addTodo,
  removeTodo,
  updateTodo,
};
