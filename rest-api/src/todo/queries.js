const getTodos = "SELECT * FROM todos";
const getTodoById = "SELECT * FROM todos WHERE id = $1";
const addTodo =
  "INSERT INTO todos (title, description, is_completed, in_progress, is_todo) VALUES ($1, $2, $3, $4, $5)";
const removeTodo = "DELETE FROM todos WHERE id = $1";
const updateTodo =
  "UPDATE todos SET title = $1, description = $2, is_completed = $3, in_progress = $4, is_todo = $5 WHERE id = $6";

module.exports = {
  getTodos,
  getTodoById,
  addTodo,
  removeTodo,
  updateTodo,
};
