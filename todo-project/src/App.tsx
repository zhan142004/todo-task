import { useEffect, useMemo, useState } from "react";
import axiosInstance from "./api/axiosInstance.js";
import { getTodos } from "./api/queries.js";

import Table, { Idata } from "./components/table/Table.js";

import SearchComponent from "./components/input/SearchComponent";
import searchIcon from "./assets/searchIcon.png";
import Button from "./components/button/Button";
import buttonIcon from "./assets/plus-add 2.png";
import RemainingTasks from "./components/remaining-tasks/RemainingTasks";
import Loading from "./components/loading/Loading.js";
import Modal from "./components/modal/Modal.js";

function App() {
  const [todoData, setTodoData] = useState<Idata[]>([]);
  const [completedTasks, setCompletedTasks] = useState<Idata[]>([]);
  const [countRemainingTasks, setCountRemainingTasks] = useState<number>(0);
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [query, setQuery] = useState("");
  const [loading, setLoading] = useState<boolean>(false);

  const getAllTodos = async () => {
    try {
      setLoading(true);
      const res = await axiosInstance.get(getTodos);

      const filterTodoTasks = res.data.filter((d: Idata) => {
        return !d.is_completed;
      });
      setTodoData(filterTodoTasks);

      const filterCompletedTasks = res.data.filter((d: Idata) => {
        return d.is_completed;
      });
      setCompletedTasks(filterCompletedTasks);

      const numOfRemainingTasks = res.data.reduce(
        (acc: number, task: Idata) => {
          if (!task.is_completed) {
            return acc + 1;
          }
          return acc;
        },
        0
      );
      setCountRemainingTasks(numOfRemainingTasks);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  const filteredAndSearchedTodo = useMemo(() => {
    return [...todoData].filter((task) => {
      return task.title.toLowerCase().includes(query);
    });
  }, [completedTasks, query]);

  const filteredAndSearchedCompleteTodo = useMemo(() => {
    return [...completedTasks].filter((task) => {
      return task.title.toLowerCase().includes(query);
    });
  }, [completedTasks, query]);

  useEffect(() => {
    getAllTodos();
  }, []);

  const onChange = (e: any) => {
    setQuery(e.target.value);
    console.log(query);
  };

  return (
    <main className="container mx-auto px-2">
      <SearchComponent
        className="my-5"
        icon={searchIcon}
        placeholder="Поиск..."
        onChange={onChange}
      />
      <section className="flex flex-col sm:flex-row gap-10 mb-5">
        <RemainingTasks remained={countRemainingTasks} />
        <Button
          onClick={() => setIsModalOpen(true)}
          icon={buttonIcon}
          size="lg"
        >
          Создать задачу
        </Button>
      </section>
      <Loading isLoading={loading} />
      <Table
        getData={getAllTodos}
        title="К Выполнению"
        data={filteredAndSearchedTodo}
      />
      <Table
        getData={getAllTodos}
        is_complete
        title="Выполненные"
        data={filteredAndSearchedCompleteTodo}
      />
      <Modal
        getData={getAllTodos}
        mode="create"
        isOpen={isModalOpen}
        setIsOpen={setIsModalOpen}
      />
    </main>
  );
}

export default App;
