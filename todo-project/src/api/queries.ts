const BASE_URL = "http://localhost:3000/api/v1/";
const getTodos = `todos/`;
const getTodoById = "todos/";
const addTodo = "todos/";
const removeTodo = "todos/";
const updateTodo = "todos/";

export { getTodos, getTodoById, addTodo, removeTodo, updateTodo };
