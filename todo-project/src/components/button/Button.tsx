import { ButtonHTMLAttributes, ReactNode } from "react";
import { twMerge } from "tailwind-merge";

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  children: ReactNode;
  size?: "lg" | "md" | "sm";
  variant?: "default" | "danger";
  icon?: string;
}

interface ButtonSizes {
  lg: string;
  md: string;
  sm: string;
}

interface ButtonVariants {
  default: string;
  danger: string;
}

const BUTTON_SIZES: ButtonSizes = {
  lg: "px-8 py-2",
  md: "px-6 py-2",
  sm: "px-4 py-2",
};

const BUTTON_VARIANTS: ButtonVariants = {
  default: "bg-[#884CB2] text-white",
  danger: "bg-red-500 text-white",
};

const Button = ({
  children,
  variant,
  size,
  icon,
  className,
  ...props
}: ButtonProps) => {
  if (!size) {
    size = "lg";
  }

  if (!variant) {
    variant = "default";
  }

  return (
    <button
      className={twMerge(
        "flex items-center gap-2 hover:bg-opacity-90 transition-all rounded-lg",
        BUTTON_VARIANTS[variant],
        BUTTON_SIZES[size],
        className
      )}
      {...props}
    >
      {icon && <img src={icon} alt="icon for button" />}
      {children}
    </button>
  );
};

export default Button;
