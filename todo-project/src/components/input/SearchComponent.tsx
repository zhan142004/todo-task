import { InputHTMLAttributes } from "react";
import { twMerge } from "tailwind-merge";

interface SearchComponentProps extends InputHTMLAttributes<HTMLInputElement> {
  icon?: string;
}

const SearchComponent = ({
  icon,
  className,
  ...props
}: SearchComponentProps) => {
  return (
    <div className="flex items-center gap-5">
      {icon && (
        <div className="hidden sm:block icon-wrapper p-1">
          <img src={icon} alt="search icon" />
        </div>
      )}
      <input
        className={twMerge("w-full sm:w-[200px]", className)}
        {...props}
        type="text"
      />
    </div>
  );
};

export default SearchComponent;
