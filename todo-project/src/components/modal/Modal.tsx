import { useEffect, useState } from "react";
import Button from "../button/Button";
import Select from "../select/Select";
import axiosInstance from "../../api/axiosInstance";
import { addTodo, updateTodo } from "../../api/queries";
import { Idata } from "../table/Table";

interface ModalProps {
  isOpen: boolean;
  setIsOpen: (newState: boolean) => void;
  mode: "edit" | "create";
  getData: () => void;
  todo?: Idata;
}

const Modal = ({ isOpen, todo, mode, getData, setIsOpen }: ModalProps) => {
  const editMode = mode === "edit";
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [selectedOption, setSelectedOption] = useState("is_todo");
  const [selectOptions, setSelectOptions] = useState([
    {
      name: "Ожидается",
      value: "is_todo",
    },
    {
      name: "Выполняется",
      value: "in_progress",
    },
    {
      name: "Выполнено",
      value: "is_completed",
    },
  ]);

  useEffect(() => {
    if (editMode && todo) {
      const chosenOption = todo.is_completed
        ? "is_completed"
        : todo.in_progress
        ? "in_progress"
        : "is_todo";
      setTitle(todo.title);
      setDescription(todo.description);
      setSelectedOption(chosenOption);
    }
  }, [todo]);

  const handleUpdate = async (e: any) => {
    e.preventDefault();
    try {
      if (todo) {
        const res = await axiosInstance.put(updateTodo + todo.id, {
          title,
          description,
          is_completed: selectedOption == "is_completed",
          in_progress: selectedOption == "in_progress",
          is_todo: selectedOption == "is_todo",
        });
        console.log(res);
        getData();
        setIsOpen(false);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const postData = async (e: any) => {
    e.preventDefault();
    console.log("post");
    try {
      const res = await axiosInstance.post(addTodo, {
        title,
        description,
        is_completed: selectedOption == "is_completed",
        in_progress: selectedOption == "in_progress",
        is_todo: selectedOption == "is_todo",
      });
      console.log(res);
      setIsOpen(false);
      getData();
    } catch (error) {
      console.error(error);
    }
  };

  if (isOpen) {
    return (
      <div
        onClick={() => setIsOpen(false)}
        className="fixed inset-0 bg-black bg-opacity-50 flex justify-center items-center"
      >
        <div
          onClick={(e) => e.stopPropagation()}
          className="bg-white w-96 h-72 p-4"
        >
          <form
            onSubmit={editMode ? handleUpdate : postData}
            className="h-full flex flex-col gap-7"
          >
            <div className="flex-grow flex flex-col gap-7">
              <input
                className="w-full p-2"
                required
                type="text"
                placeholder="Title"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
              />
              <input
                className="w-full p-2"
                required
                type="text"
                placeholder="Description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
              <Select
                selected={selectedOption}
                setSelectedOption={setSelectedOption}
                options={selectOptions}
              />
            </div>
            <div className="flex justify-between">
              <Button
                onClick={() => setIsOpen(false)}
                type="button"
                variant="danger"
              >
                Назад
              </Button>
              <Button type="submit" className="">
                {editMode ? "Сохранить" : "Создать"}
              </Button>
            </div>
          </form>
        </div>
      </div>
    );
  }
};

export default Modal;
