import { twMerge } from "tailwind-merge";

interface RemainingTasksProps {
  remained: number;
  className?: string;
}

const RemainingTasks = ({ remained, className }: RemainingTasksProps) => {
  return (
    <h2 className={twMerge("text-2xl sm:text-3xl font-bold", className)}>
      Осталось невыполненных{" "}
      <span className="text-red-500">{remained} задачи</span>
    </h2>
  );
};

export default RemainingTasks;
