interface Ioptions {
  name: string;
  value: string;
}

interface SelectProps {
  selected: string;
  options: Ioptions[];
  setSelectedOption: (newValue: string) => void;
}

const Select = ({ selected, setSelectedOption, options }: SelectProps) => {
  return (
    <select
      onChange={(e) => setSelectedOption(e.target.value)}
      value={selected}
    >
      {options.map((option) => (
        <option key={option.name} value={option.value}>
          {option.name}
        </option>
      ))}
    </select>
  );
};

export default Select;
