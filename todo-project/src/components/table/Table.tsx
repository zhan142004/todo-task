import { twMerge } from "tailwind-merge";

import tableIcon from "../../assets/Group 212.1.png";
import deleteIcon from "../../assets/Vectordelete.png";
import editIcon from "../../assets/edit-3edit.png";

import Button from "../button/Button";
import axiosInstance from "../../api/axiosInstance";
import { removeTodo } from "../../api/queries";
import Modal from "../modal/Modal";
import { useState } from "react";

export interface Idata {
  id: string;
  title: string;
  description: string;
  is_completed: boolean;
  in_progress: boolean;
  is_todo: boolean;
}

interface TableProps {
  title?: string;
  data: Idata[];
  className?: string;
  is_complete?: boolean;
  getData: () => void;
}

const Table = ({
  title,
  data,
  is_complete,
  getData,
  className,
}: TableProps) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [currentTodo, setCurrentTodo] = useState<Idata>({
    id: "1",
    title: "",
    description: "",
    is_completed: true,
    in_progress: false,
    is_todo: false,
  });
  if (!data.length) {
    return <h2>There is no data for table</h2>;
  }

  const deleteTodo = async (id: number) => {
    try {
      const res = await axiosInstance.delete(removeTodo + id);
      console.log(res);
      getData();
    } catch (error) {
      console.error(error);
    }
  };

  const handleEdit = async (todo: Idata) => {
    setCurrentTodo(todo);
    setIsModalOpen(true);
  };

  return (
    <>
      <Modal
        isOpen={isModalOpen}
        setIsOpen={setIsModalOpen}
        getData={getData}
        mode="edit"
        todo={currentTodo}
      />
      <h2 className="text-3xl my-2">
        {title}{" "}
        {is_complete ? (
          <span className="px-4 py-1 text-lg bg-red-500 bg-opacity-20 text-red-500 inline-block rounded-lg">
            Не Активно
          </span>
        ) : (
          ""
        )}
      </h2>
      <table className={twMerge("w-full", className)}>
        <tbody>
          {data.map((d) => (
            <tr key={d.id} className="font-sans border-b last:border-none">
              <td className="p-2 block sm:table-cell">
                <div className="flex items-center gap-2">
                  <span>
                    <img src={tableIcon} alt="" />
                  </span>{" "}
                  <h2
                    className={`text-2xl ${
                      d.is_completed ? "text-[#BDBDBD]" : ""
                    }`}
                  >
                    {d.title}
                  </h2>
                </div>
                <p className="sm:hidden">{d.description}</p>
              </td>
              <td
                className={`p-2 hidden sm:table-cell ${
                  d.is_completed ? "text-[#BDBDBD]" : ""
                }`}
              >
                {d.description}
              </td>
              <td className="p-2 block sm:table-cell">
                <span
                  className={`
                    px-2
                    py-1
                    inline-block
                    rounded-xl
                    ${
                      d.is_completed
                        ? "bg-[#27AE60] bg-opacity-20 text-[#27AE60]"
                        : d.in_progress
                        ? "bg-[#56CCF2] bg-opacity-20 text-[#56CCF2]"
                        : d.is_todo
                        ? "bg-[#F2994A] bg-opacity-20 text-[#F2994A]"
                        : ""
                    }
                  `}
                >
                  {d.is_completed
                    ? "Выполнено"
                    : d.in_progress
                    ? "В Прогрессе"
                    : d.is_todo
                    ? "Ожидается"
                    : ""}
                </span>
              </td>
              <td className="p-2 block sm:table-cell">
                <div className="flex items-center justify-end gap-10">
                  <button onClick={() => handleEdit(d)}>
                    <img width={24} src={editIcon} alt="" />
                  </button>
                  <button onClick={() => deleteTodo(parseInt(d.id))}>
                    <img width={24} src={deleteIcon} alt="" />
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
};

export default Table;
